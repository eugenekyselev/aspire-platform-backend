exports.options = {
    routePrefix: '/documentation',
    exposeRoute: true,
    swagger: {
        info: {
            title: 'Aspire API',
            description: '',
            version: '1.0.0'
        },
        host: process.env.NODE_ENV === 'development' ? 'localhost:4000' : 'ancient-inlet-80057.herokuapp.com',
        schemes: process.env.NODE_ENV === 'development' ? ['http'] : ['https'],
        consumes: ['application/json'],
        produces: ['application/json'],
        tags: [
            { name: 'profile' },
            { name: 'users' },
            { name: 'authentication' }
        ],
    }
}