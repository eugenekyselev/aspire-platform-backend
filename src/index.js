const path = require('path');

// Require the framework and instantiate it
const fastify = require('fastify')({
    logger: true
})

// auth plugin
const oauthPlugin = require('fastify-oauth2')

// Require autoload plugins
const AutoLoad = require('fastify-autoload')

// Require external modules
const mongoose = require('mongoose')

// Connect to DB
mongoose.connect('mongodb://admin:Aspire987@ds253104.mlab.com:53104/aspire')
    .then(() => console.log('MongoDB connected…'))
    .catch(err => console.log(err))

// Import Swagger Options
const swagger = require('./config/swagger')

// Register Swagger
fastify
    .register(require('fastify-swagger'), swagger.options)
    .register(require('fastify-multipart'), {
        limits: {
            files: 1
        }
    })
    .register(require('fastify-jwt'), { secret: 'supersecret' })
    .register(require('fastify-auth'))
    .register(require('fastify-cors'), { origin: ['http://localhost:3000', 'https://aspireapp.gitlab.io'], exposedHeaders: 'authorization', methods: ['GET', 'PUT', 'POST'] })
    .register(require('./plugins/verifyJWT'))
    .register(require('./plugins/verifyUniqueUser'))
    .register(require('./plugins/verifyUserAndPassword'))
    .register(AutoLoad, {
        dir: path.join(__dirname, 'routes')
    })
    .register(oauthPlugin, {
        name: 'linkedinOAuth2',
        credentials: {
            client: {
                id: '77c4tf4tfcmxta',
                secret: 'NDRnePCWQGXQiPtb'
            },
            auth: oauthPlugin.LINKEDIN_CONFIGURATION
        },
        scope: 'r_liteprofile,r_emailaddress',
        startRedirectPath: '/login/linkedin',
        callbackUri: process.env.NODE_ENV === 'development' ? 'http://localhost:4000/login/linkedin/callback' : 'https://ancient-inlet-80057.herokuapp.com/login/linkedin/callback'
    })

// Run the server!
const start = async () => {
    try {
        process.env.NODE_ENV === 'development' ? await fastify.listen(4000) : await fastify.listen(process.env.PORT, '0.0.0.0')
        fastify.swagger()
        fastify.log.info(`server listening on ${fastify.server.address().port}`)
    } catch (err) {
        fastify.log.error(err)
        process.exit(1)
    }
}
start()