const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    role: String,
    linkedinId: String,
    email: String,
    pwd: String,
    data: Object
})

module.exports = mongoose.model('User', userSchema)