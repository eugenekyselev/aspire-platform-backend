// External Dependancies
const boom = require('boom');
const _ = require('lodash');

// Get Data Models
const User = require('../models/User')

// Import Swagger documentation
const documentation = require('../documentation/userApi')

const me = async function (fastify, opts) {
    fastify.route({
        method: 'GET',
        url: '/api/me',
        beforeHandler: fastify.auth([fastify.verifyJWT]),
        handler: async (req, reply) => {
            try {
                const decoded = await fastify.jwt.verify(req.headers['authorization'])
                const user = await User.findById(decoded.id)
                reply.send(_.pick(user, ['id', 'email', 'role', 'data']))
            } catch (err) {
                throw boom.boomify(err)
            }
        },
        schema: documentation.getMeSchema
    })
} 

module.exports = me;