// External Dependancies
const boom = require('boom');

// Get Data Models
const User = require('../models/User')

// Import Swagger documentation
const documentation = require('../documentation/userApi')

const users = async function (fastify, opts) {
    fastify.route({
        method: 'GET',
        url: '/api/users',
        handler: async (req, reply) => {
            try {
                const users = await User.find()
                return users
            } catch (err) {
                throw boom.boomify(err)
            }
        },
        schema: documentation.getUsersSchema
    })
} 

module.exports = users;