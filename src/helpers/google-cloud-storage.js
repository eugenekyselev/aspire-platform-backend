const { Storage } = require('@google-cloud/storage')
  
const GOOGLE_CLOUD_PROJECT_ID = 'aspire-mvp-1536973199996'; 
const GOOGLE_CLOUD_KEYFILE = './gcs.json';

const storage = new Storage({
    projectId: GOOGLE_CLOUD_PROJECT_ID,
    keyFilename: GOOGLE_CLOUD_KEYFILE,
});

module.exports = storage;