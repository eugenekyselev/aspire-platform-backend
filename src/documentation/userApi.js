exports.getUsersSchema = {
    description: 'Get all users',
    summary: 'Gets all existing users (for testing purpose only)',
    tags: ['users']
}

exports.updateMeSchema = {
    description: 'Update user',
    summary: 'Updates user with given values',
    tags: ['profile'],
    headers: {
        type: 'object',
        properties: {
            authorization: {
                type: 'string',
                description: 'Token'
            }
        },
        required: ['authorization']
    },
    body: {
        type: 'object',
        properties: {
            email: { type: 'string' },
            data: { type: 'object' }
        }
    }
}

exports.getMeSchema = {
    description: 'Get profile',
    summary: 'Get user profile information',
    tags: ['profile'],
    headers: {
        type: 'object',
        properties: {
            authorization: {
                type: 'string',
                description: 'Token'
            }
        },
        required: ['authorization']
    }
}

exports.registerSchema = {
    description: 'Register',
    summary: 'Register',
    tags: ['authentication'],
    body: {
        type: 'object',
        properties: {
            role: { type: 'string' },
            email: { type: 'string' },
            pwd: { type: 'string' },
            data: { type: 'object' }
        },
        required: ['email','pwd','role']
    }
}

exports.loginSchema = {
    description: 'Login',
    summary: 'Login',
    tags: ['authentication'],
    body: {
        type: 'object',
        properties: {
            email: { type: 'string' },
            pwd: { type: 'string' },
            role: { type: 'string' }
        },
        required: ['email','pwd','role']
    }
}


exports.uploadSchema = {
    description: 'Upload',
    summary: 'Upload file',
    tags: ['profile'],
    consumes: ['multipart/form-data'],
    headers: {
        type: 'object',
        properties: {
            authorization: {
                type: 'string',
                description: 'Token'
            }
        },
        required: ['authorization']
    },
    body: {
        type: 'object',
        properties: {
            file: {
                type: 'file'
            }
        },
        required: ['file']
    }
}